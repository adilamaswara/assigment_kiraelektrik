<!DOCTYPE html>
<html>
<head>
    <title>Power Calculator</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>Calculate</h1>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <div class="form-group">
                <label for="current">Voltage</label>
                <input type="number" step="0.01" name="voltage" id="voltage" class="form-control" required>
                <small for="current">Voltage (V)</small>
            </div>
            <div class="form-group">
                <label for="current">Current</label>
                <input type="number" step="0.01" name="current" id="current" class="form-control" required>
                <small for="current">Ampere (A)</small>
            </div>
            <div class="form-group">
                <label for="rate">CURRENT RATE</label>
                <input type="number" step="0.01" name="rate" id="rate" class="form-control" required>
                <small for="current">sen/kWh</small>
            </div>
            <center><button type="submit" class="btn btn-primary">Calculate</button></center>
        </form>
        <br>
        <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $current = $_POST['current'];
            $rate = $_POST['rate'];
            $voltage = $_POST['voltage'];

            // Calculate power (P = I * V)
            $power = $current * $voltage;

            // Calculate energy (E = P * t, assuming t = 1 hour)
            $energy = $power * 24 *1000;

            // Calculate total charge (C = E * rate)
            $charge = $rate;

            echo "<h3>Results:</h3>";
            echo "<p>POWER: " . $power/1000 . " kw</p>";
            echo "<p>RATE: RM" . $charge/100 . "</p>";
        }
        ?>
    </div>
</body>
</html>
